import React, { useEffect, useState } from 'react';

// from React importing a hook called 'useEffect'

function LocationForm () {
    // making Location Form appear so that user can input data


    // DATA INPUT
    // NAME OF LOCATION
    const [name, setName] = useState('');
    // Set the useState hook for "Name"
    // Store what the user inputs into "Name" into the component's state, with a default initial value of an empty ''
    const handleNameChange = (event) => {
    // Takes what the user inputs into "Name" section of the form and stores it in the state's "name" variable.
        const value = event.target.value;
        // event.target.value = the text that the user typed into the form for location "Name"
        setName(value);
    }
    // ROOM COUNT
    const [roomCount, setRoomCount] = useState('');
    // Set useState hook for "Room Count"
    // Store what the user inputs into "Room Count" into the component's state, with a default initial value of an empty ''
    const handleRoomCountChange = (event) => {
        const value = event.target.value
        // event.target.value = the text that the user typed into the form for location "Room Count"
        setRoomCount(value)
        // each time update Room Count, hook will trigger and the room count will update (it is a number string)
    }
    // CITY
    const [city, setCity] = useState('')
    // Set useState hook for "City"
    // Store what the user inputs into "City" into the component's state, with a default initial value of an empty ''
    const handleCityChange = (event) => {
        const value = event.target.value
        // event.target.value = the text that the user typed into the form for location "City"
        setCity(value)
        // each time update City, hook will trigger and the city will update
    }
    // STATE (ALASKA, ARIZONA, ETC.)
    const [state, setState] = useState('')
    // Set useState hook for "STATE"
    const handleStateChange = (event) => {
        const value = event.target.value
        // event.target.value = the text that the user chose in the drop down menu
        setState(value)
        // update the STATE
    }

    // FORM SUBMISSION
    const handleSubmit = async (event) => {
    // Deal with submission of the form
        event.preventDefault()
        // Don't refresh page when form is submitted

        const data = {}
        // create empty JSON object with the below keys and values

        data.room_count = roomCount
        data.name = name
        data.city = city
        data.state = state
        // console.log(data)

        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            // post the location to the above URL
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
        // wait for this info to come back
        if (response.ok){
          // if response is ok, create new location
            const newLocation = await response.json()
            console.log(newLocation)
            // to see if location is created
        }
    }
    // GET STATES FROM API AND PUT THEM INTO ARRAY
    const [states, setStates] = useState([])
    // the point of this hook is to populate all the states in the list
    // it gives the user States in the drop down menu
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        // fetch all states from API
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setStates(data.states)
          // call function from line 84 and put the states into that empty list
        }
    }

    useEffect(() => {
        //this Hook tells React that your component needs to do something after render
        // this is a pre-made function that we are calling
        // passing two functions, arrow function and empty array aka empty Dependency array (called 1x)
        // without the dependency array would be in an infinite loop
    fetchData();
    }, []);
    // useEffect, empty [] means execute 1x and will execute 1x and literally fetch data


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={value.name} placeholder="Name" required type="text" name ="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={handleStateChange} required name="state" id ="state" className="form-select">
                  <option value="">Choose a state</option>
                  {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm
