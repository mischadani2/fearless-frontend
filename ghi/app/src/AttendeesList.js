import React from 'react'

function AttendeesList(props) {
    console.log(props)
// This function displays the table of attendees
// import the table int App.js so that we can see the table
    return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Conference</th>
      </tr>
    </thead>
    <tbody>
      {/* for (let attendee of props.attendees) {
      <tr>
          <td>{ attendee.name }</td>
          <td>{ attendee.conference }</td>
        </tr>
      } */}
      {props.attendees.map(attendee => {
        return (
            //
    // props is called here -- need to pass in as a parameter
    // this whole HTML section used to be in App.js inside
        <tr key={attendee.href}>
          <td>{ attendee.name }</td>
          <td>{ attendee.conference }</td>
        </tr>
        )
      })}
    </tbody>
  </table>
  )

}

export default AttendeesList
